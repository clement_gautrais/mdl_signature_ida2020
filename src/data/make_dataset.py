import numpy as np
import urllib.request
import tarfile
import pandas as pd
import json
import os
from lxml import html


def generate_signature_add_rates(nb_segments, segment_lengths, nb_products, noise_rate, signature_add_rate, noise_items_ratio=1):
    """
    Generate a customer with a planted signature.
    The number of segments is drawn from a uniform law between the specified limits.
    The segment lengths are drawn from a uniform law between the specified limits.
    The signature contains nb_products
    Noise items are added according to a uniform law. The number of noise items is the same as the signature size and the add_rate control the proportion of noise items in each segment.
    Signature items can be forgotten at a forget rate. Each segment contains (1-forget_rate)*nb_products signature items
    :param nb_segments: A tuple, delimiting the number of segments limits
    :param segment_lengths: A tuple, delimiting the size of each segment
    :param nb_products: The number of signature products (and also noise items)
    :param noise_rate: The rate at which noise items should be added to each segment
    :param signature_add_rate: The rate at which signature items can be added from a segment.
    :return: A triplet. First element is the customer baskets, second is the signature products and last is the segment length
    """
    prod_ids = range(1000, 1000 + nb_products)
    added_prod_ids = range(3000, 3000 + nb_products*noise_items_ratio)
    cust = {}
    cust['baskets'] = []
    nbs_segments = np.random.choice(range(nb_segments[0], nb_segments[1] + 1), size=1)[0]
    segments_lengths = np.random.choice(range(segment_lengths[0], segment_lengths[1] + 1),
                                     size=nbs_segments)
    for s_len in segments_lengths:
        # We now sample the products of this block according to the parameters
        products = np.random.choice(prod_ids, size=int(nb_products), replace=False)
        added_sign_products = np.random.choice(prod_ids, size=int(nb_products*signature_add_rate), replace=True)
        noise_products = np.random.choice(added_prod_ids, size=int(nb_products*noise_rate), replace=True)
        segment_prods = np.concatenate((products, added_sign_products, noise_products))
        np.random.shuffle(segment_prods)

        for i, prods in enumerate(np.array_split(segment_prods, s_len)):
            my_prods = prods.tolist()
            cust['baskets'].append({'timestamp': len(cust['baskets']) + 1, 'products': [int(p) for p in my_prods]})
    return cust, list(prod_ids), segments_lengths


def build_instacart_json_file():
    dataset_path = "./instacart_2017_05_01"

    final_json_file = os.path.join(dataset_path, "all_customers.jsonl")

    if os.path.exists(final_json_file):
        print("Existing customer file, skipping customer file creation")

    else:
        orders = pd.read_csv(os.path.join(dataset_path, "orders.csv"))
        orders_products = pd.read_csv(os.path.join(dataset_path, "order_products__prior.csv"))
        full_order_info = pd.merge(orders, orders_products, on=["order_id"])[["order_id", "user_id", "order_number", "product_id"]]
        grouped_orders = full_order_info.groupby(["user_id", "order_number"])["product_id"].apply(list)
        sorted_orders = grouped_orders.reset_index().sort_values(by=["user_id", "order_number"])

        cust_orders = sorted_orders.groupby(["user_id"])

        with open(final_json_file, "w") as f:
            for i, g in cust_orders:
                cust = {}
                cust["client_id"] = i
                cust["baskets"] = [{"timestamp": j + 1, "products": p} for j, p in enumerate(g["product_id"])]
                f.write(json.dumps(cust) + "\n")


def download_instacart_dataset():
    tar_filename = "./instacart_data.tar.gz"
    if os.path.exists(tar_filename):
        print("Existing tar.gz file, skipping download")
    else:
        print("Getting dataset link")
        dataset_link = "https://www.instacart.com/datasets/grocery-shopping-2017"
        tree = html.fromstring(urllib.request.urlopen(dataset_link).read())
        buttons = tree.xpath("//*[contains(@class, 'ic-btn ic-btn-success ic-btn-lg')]")
        if not buttons:
            print("Unable to find dataset link, please download manually the Instacart dataset and copy manually the instacart_data.tar.gz file in the data directory.")
        else:
            download_link = buttons[0].attrib["href"]
            print("Got dataseet link, downloading now")
            instacart_filedata = urllib.request.urlopen(download_link)
            targz_data = instacart_filedata.read()

            with open(tar_filename, "wb") as f:
                f.write(targz_data)

            print("Download done")
    print("Uncompressing {}".format(tar_filename))

    expected_dir = "./instacart_2017_05_01"
    if os.path.exists(expected_dir):
        print("Existing uncompressed directory, skipping uncompress data file")
    else:
        tar = tarfile.open(tar_filename, "r:gz")
        tar.extractall()
        tar.close()


if __name__ =="__main__":
    prev_dir = os.getcwd()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    print("Downloading Instacart data")
    download_instacart_dataset()
    print("Building json customer file (this will take around 5 minutes, and about 6 Gb of RAM at most)")
    build_instacart_json_file()
    print("Finished building customer file")
    os.chdir(prev_dir)

