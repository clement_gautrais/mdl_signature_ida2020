import os
import json
import argparse
import sys
import time
import subprocess

sys.path.append(os.getcwd())

from src.mining.signature_mining import compute_hybrid_mdl_signature, compute_widening_mdl_signature
from src.data.make_dataset import generate_signature_add_rates

parser = argparse.ArgumentParser()
parser.add_argument("--expe_params_json", dest="expe_params_json", type=str)
parser.add_argument('--cust_index', dest='cust_index', type=int, help='Index of the customer to process')
args = parser.parse_args()

expe_params_json = args.expe_params_json
with open(expe_params_json) as f:
    expe_params = json.load(f)

if "include_dir" in expe_params.keys():
    if expe_params["include_dir"] not in sys.path:
        sys.path.insert(0, expe_params["include_dir"])

signature_jar_path = expe_params["signature_jar_path"]

for noise_rate in expe_params["noise_rates"]:
    for add_rate in expe_params["signature_add_rates"]:
        for items_ratio in expe_params["noise_items_ratio"]:
            c, sign_prods, seg_length = generate_signature_add_rates([expe_params["min_nb_segments"], expe_params["max_nb_segments"]], [expe_params["min_segment_lengths"], expe_params["max_segment_lengths"]], expe_params["nb_products"], noise_rate, add_rate, items_ratio)
            with open(os.path.join(expe_params["output_dir"], str(args.cust_index) + "_noise=" + str(noise_rate) + "_add=" + str(add_rate) + "_ratio=" + str(items_ratio) + ".jsonl"), "w") as output_file:
                output_file.write(json.dumps({"baskets":c["baskets"], "sign_prods":sign_prods, "seg_length":seg_length.tolist()}) + "\n")
                try:
                    for encoding in expe_params["encodings"]:
                        for method in expe_params["methods"]:
                            if method in ["hybrid", "dynProg", "dynProgMax"]:
                                start = time.time()
                                best_sign = compute_hybrid_mdl_signature(c["baskets"],
                                                                                        signature_jar_path,
                                                                                        method=method,
                                                                                        encoding_function=encoding,
                                                                                        timeout=expe_params["timeout"],
                                                                                        option="-optimize")
                                stop = time.time()
                                output_file.write(
                                    json.dumps({"client_id": args.cust_index, "full_signature": best_sign, "encoding": encoding,
                                                "method": method, "beam_width": -1, "beta": -1, "runtime": stop - start, "noise_rate":noise_rate, "add_rate":add_rate, "items_ratio":items_ratio})
                                    + "\n")
                            elif method in ["widening"]:
                                for beam_width in expe_params["beam_widths"]:
                                    for beta in expe_params["betas"]:
                                        start = time.time()
                                        best_sign = compute_widening_mdl_signature(c["baskets"],
                                                                                                  signature_jar_path,
                                                                                                  beam_width=beam_width,
                                                                                                  beta=beta,
                                                                                                  encoding_function=encoding,
                                                                                                  timeout=expe_params["timeout"])
                                        stop = time.time()
                                        output_file.write(
                                            json.dumps({"client_id": args.cust_index, "full_signature": best_sign, "encoding": encoding,
                                                        "method": method, "beam_width": beam_width, "beta": beta,
                                                        "runtime": stop - start, "noise_rate":noise_rate, "add_rate":add_rate, "items_ratio":items_ratio})
                                            + "\n")
                except subprocess.TimeoutExpired:
                    output_file.write(json.dumps({"noise_rate":noise_rate, "add_rate":add_rate, "items_ratio":items_ratio, "method":"timeout"}))

