import os
import json
import datetime

expe_params_directory = "src/experiments/expe_parameters/"

with open(os.path.join(expe_params_directory,"mdl_encoding_comparison_base.json")) as f:
    expe_params = json.load(f)

    now = datetime.datetime.now()

    expe_params["output_dir"] = expe_params["output_dir"] + str(now.year) + "_" + str(now.month) + "_" + str(now.day) + "_" + str(now.hour) + "_" + str(now.minute)

    os.makedirs(expe_params["output_dir"], exist_ok=True)

    with open(os.path.join(expe_params["output_dir"], "expe_params.json"), "w") as param_file:
        json.dump(expe_params, param_file)

    gnu_parallel = input("Can you use GNU parallel on your computer? yes[y] or no[n]")
    if gnu_parallel[0] == "y" or gnu_parallel[0] == "Y":
        print("You have GNU parallel: run the following command")
        print("parallel 'python " + os.path.join(expe_params["script_directory"], "mdl_encoding_comparison.py") + " --cust_index {} --expe_params_json " + os.path.join(expe_params["output_dir"], "expe_params.json") + "' ::: {1.." + str(expe_params["nb_custs"]) + "}")


    else:
        import subprocess
        print("You do not have GNU parallel, running experiments through python loop (this will take a while...)")
        for i in range(1, expe_params["nb_custs"]+1):
            subprocess.call(["python", os.path.join(expe_params["script_directory"], "mdl_encoding_comparison.py"),
                             "--cust_index", str(i), "--expe_params_json", os.path.join(expe_params["output_dir"], "expe_params.json")])