import os
import json
import argparse
import sys
import time

sys.path.append(os.getcwd())

from src.mining.signature_mining import compute_hybrid_mdl_signature, compute_widening_mdl_signature, compute_empty_model_mdl


parser = argparse.ArgumentParser()
parser.add_argument('--cust_index', dest='cust_index', type=int, help='Index of the customer to process')
parser.add_argument("--expe_params_json", dest="expe_params_json", type=str)
args = parser.parse_args()

expe_params_json = args.expe_params_json
with open(expe_params_json) as f:
    expe_params = json.load(f)

if "include_dir" in expe_params.keys():
    if expe_params["include_dir"] not in sys.path:
        sys.path.insert(0, expe_params["include_dir"])

signature_jar_path = expe_params["signature_jar_path"]

user_data = {}
current_nb_custs = 1
with open(expe_params["dataset_path"]) as f:
    for l in f:
        user = json.loads(l)
        if len(user["baskets"]) > expe_params["min_nb_baskets"]:
            if current_nb_custs == args.cust_index:
                user_data = user
                break
            else:
                current_nb_custs += 1

user_data["baskets"] = [b for b in user_data["baskets"] if b["products"]]

with open(os.path.join(expe_params["output_dir"], str(args.cust_index) + ".jsonl"), "w") as output_file:
    for encoding in expe_params["encodings"]:
        mdl_empty_model_cost = compute_empty_model_mdl(user_data["baskets"],
                                                                      signature_jar_path,
                                                                      encoding_function=encoding)
        for method in expe_params["methods"]:
            if method in ["hybrid", "dynProg", "dynProgMax"]:
                start = time.time()
                best_sign = compute_hybrid_mdl_signature(user_data["baskets"],
                                                                        signature_jar_path,
                                                                        method=method,
                                                                        encoding_function=encoding,
                                                                        option="-optimize")
                stop = time.time()
                output_file.write(json.dumps({"client_id" : user_data["client_id"], "full_signature": best_sign,"encoding": encoding,
                                             "empty_model_length": mdl_empty_model_cost, "method": method,
                                              "beam_width": -1, "beta": -1, "runtime": stop-start})
                                  + "\n")
            elif method in ["widening"]:
                for beam_width in expe_params["beam_widths"]:
                    for beta in expe_params["betas"]:
                        start = time.time()
                        best_sign = compute_widening_mdl_signature(user_data["baskets"],
                                                                                  signature_jar_path,
                                                                                  beam_width=beam_width,
                                                                                  beta=beta,
                                                                                  encoding_function=encoding)
                        stop = time.time()
                        output_file.write(json.dumps({"client_id" : user_data["client_id"], "full_signature": best_sign, "encoding": encoding,
                                                      "empty_model_length": mdl_empty_model_cost, "method": method,
                                                      "beam_width": beam_width, "beta": beta, "runtime": stop-start})
                                          + "\n")
