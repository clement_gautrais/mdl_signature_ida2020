import os
import subprocess
import uuid


def compute_widening_mdl_signature(baskets, signature_jar_path, beam_width=1, beta=0, encoding_function="arrangement", timeout=None):
    """
    Computes the signature with the lowest code length, for the given list of baskets. It uses the widening algorithm.
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
        It can also be a file name corresponding to a dataset. The dataset is formatted as follows: Each transaction is a line, with comma separated items id.
    :param signature_jar_path: The path to the signature jar
    :param beam_width: Size of the beam.
    :param beta: Diversity parameter. Float number between 0 and 1. 0 means no diversity (classic beam search).
    :param encoding_function: The encoding function to use. Default is arrangement, the one presented in the paper. Combination for using combination of item occurrences, arrangement for using arrangement of item occurrences and indicator for indicator values of transactions
    :return: The best MDL signature found by the widening algorithm (lowest MDL cost).
    """
    working_dir = os.getcwd()
    rand_id = uuid.uuid4()
    dataset_base_name = "temp_signature_dataset" + str(rand_id)
    dataset_name = os.path.join(working_dir, dataset_base_name)
    output_base_name = "temp_signature_result" + str(rand_id)
    output_name = os.path.join(working_dir, output_base_name)

    # If we are given a list of baskets, we create a temporary dataset
    if not isinstance(baskets, str):
        with open(dataset_name, "w+") as dataset:
            for b in baskets:
                dataset.write(",".join([str(p) for p in b['products']]) + "\n")
    # If we have a filename, we use the file as the dataset
    else:
        dataset_name = baskets

    runner = "fr.liglab.datalyse.retail.runners.RunMDLSignatureWideningFinder"
    try:
        ret_code = subprocess.call(["java", "-cp", os.path.abspath(signature_jar_path),
                         runner,
                         "-input", os.path.abspath(dataset_name),
                         "-w", str(beam_width),
                         "-beta", str(beta),
                         "-splitStream", str(1),
                         "-noiseEncoding", "uniform",
                         "-signatureNoiseEncoding", "uniform",
                         "-encodingFunction", encoding_function,
                         "-output", output_name], timeout=timeout)
    except subprocess.TimeoutExpired:
        raise subprocess.TimeoutExpired
    else:
        signature = {}
        if ret_code == 0:
            with open(output_name, "r") as output:
                for line in output:
                    if len(line.rstrip().split(":")) > 1:
                        blocs = [list(map(int, b.split(";"))) for b in line.rstrip().split(":")[0].split(",")]
                        items = []
                        if line.rstrip().split(":")[1]:
                            items = list(map(int, line.rstrip().split(":")[1].split(",")))
                        signature = {"blocs": blocs, "signature": items, "cost": float(line.rstrip().split(":")[2])}

            return signature

    finally:
        os.remove(dataset_name)
        os.remove(output_name)


def compute_hybrid_mdl_signature(baskets, signature_jar_path, method="dynProgMax", encoding_function="arrangement", timeout=None, option=""):
    """
    Computes the signature with the lowest code length, for the given list of baskets. It uses the naive algorithm.
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
        It can also be a file name corresponding to a dataset. The dataset is formatted as follows: Each transaction is a line, with comma separated items id.
    :param signature_jar_path: The path to the signature jar
    :param method: The method to use. Either using the dynamic programming algorithm (dynProg) or the dynamic programming using max (dynProgMax).
    :param encoding_function: The encoding function to use. Default is arrangement, the one presented in the paper. Combination for using combination of item occurrences, arrangement for using arrangement of item occurrences and indicator for indicator values of transactions
    :return: The best MDL signature found by the naive algorithm (lowest MDL cost).
    """
    working_dir = os.getcwd()
    rand_id = uuid.uuid4()
    dataset_base_name = "temp_signature_dataset" + str(rand_id)
    dataset_name = os.path.join(working_dir, dataset_base_name)
    output_base_name = "temp_signature_result" + str(rand_id)
    output_name = os.path.join(working_dir, output_base_name)

    # If we are given a list of baskets, we create a temporary dataset
    if not isinstance(baskets, str):
        with open(dataset_name, "w+") as dataset:
            for b in baskets:
                dataset.write(",".join([str(p) for p in b['products']]) + "\n")
    # If we have a filename, we use the file as the dataset
    else:
        dataset_name = baskets

    runner = "fr.liglab.datalyse.retail.runners.RunMDLSignatureSkylineFinder"

    try:
        ret_code = subprocess.call(["java", "-cp", os.path.abspath(signature_jar_path),
                         runner,
                         option,
                         "-input", os.path.abspath(dataset_name),
                         "-splitStream", str(1),
                         "-method", method,
                         "-noiseEncoding", "uniform",
                         "-signatureNoiseEncoding", "uniform",
                         "-encodingFunction", encoding_function,
                         "-output", output_name], timeout=timeout)
    except subprocess.TimeoutExpired:
        raise subprocess.TimeoutExpired
    else:
        signature = {}
        if ret_code == 0:
            with open(output_name, "r") as output:
                for line in output:
                    if len(line.rstrip().split(":")) > 1:
                        blocs = [list(map(int, b.split(";"))) for b in line.rstrip().split(":")[0].split(",")]
                        items = []
                        if line.rstrip().split(":")[1]:
                            items = list(map(int, line.rstrip().split(":")[1].split(",")))
                        signature = {"blocs": blocs, "signature": items, "cost": float(line.rstrip().split(":")[2])}
            return signature
    finally:
        os.remove(dataset_name)
        os.remove(output_name)


def compute_empty_model_mdl(baskets, signature_jar_path, encoding_function="arrangement"):
    """
    Computes the code length of the empty model (empty signature)
    :param baskets: A list of baskets. Each basket is a dict with 2 keys : timestamp and products. Products is a list of products id.
        It can also be a file name corresponding to a dataset. The dataset is formatted as follows: Each transaction is a line, with comma separated items id.
    :param signature_jar_path: The path to the signature jar
    :param encoding_function: The encoding function to use. Default is arrangement, the one presented in the paper. Combination for using combination of item occurrences, arrangement for using arrangement of item occurrences and indicator for indicator values of transactions
    :return: The code length of the empty model (in bits)
    """
    working_dir = os.getcwd()
    rand_id = uuid.uuid4()
    dataset_base_name = "temp_signature_dataset" + str(rand_id)
    dataset_name = os.path.join(working_dir, dataset_base_name)
    output_base_name = "temp_signature_result" + str(rand_id)
    output_name = os.path.join(working_dir, output_base_name)

    # If we are given a list of baskets, we create a temporary dataset
    if not isinstance(baskets, str):
        with open(dataset_name, "w+") as dataset:
            for b in baskets:
                dataset.write(",".join([str(p) for p in b['products']]) + "\n")
    # If we have a filename, we use the file as the dataset
    else:
        dataset_name = baskets

    runner = "fr.liglab.datalyse.retail.runners.EmptyModelCodeLength"

    ret_code = subprocess.call(["java", "-cp", os.path.abspath(signature_jar_path),
                     runner,
                     "-input", os.path.abspath(dataset_name),
                     "-splitStream", str(1),
                     "-noiseEncoding", "uniform",
                     "-signatureNoiseEncoding", "uniform",
                     "-encodingFunction", encoding_function,
                     "-output", output_name])
    length = -1
    if ret_code == 0:
        with open(output_name, "r") as output:
            for line in output:
                length = float(line.strip())

    os.remove(dataset_name)
    os.remove(output_name)

    return length