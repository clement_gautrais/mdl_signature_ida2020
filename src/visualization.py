import seaborn
import matplotlib.pyplot as plt


def get_signature_plot(signature, baskets, token_dict, basket_increment=10, figsize=(12,4)):
    """
    Returns Figure and Axis corresponding to a plot for the signature occurrences
    :param signature: Signature to plot: this is a dict with 2 keys: blocs and products. Products contains a list of integers, blocs contains a list of tuples containing segment limits.
    :param baskets: List of baskets (list of dict with 2 keys: timestamp and products).
    :param token_dict: Dictionary to map integers to string names (for example, product taxonomy)
    :param basket_increment: Increment of the xaxis (number of transactions)
    :param figsize: Tuple containing the figure size (passed to plt.subplots())
    :return:
    """
    seaborn.set_context("talk")

    prods = signature["signature"]
    # TODO: Maybe add the top-k as well for comparison?
    # all_occurrences = [p for b in baskets for p in b["products"]]
    # from collections import Counter

    # prods = [p[0] for p in Counter(all_occurrences).most_common(10)]

    real_names = [token_dict[p] for p in prods]

    fig, ax = plt.subplots(figsize=figsize)

    plt.rc('xtick', labelsize=20)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=20)

    ax.set_yticks([1 + 0.2 * i for i in range(len(prods))])
    ax.set_yticklabels(real_names)
    ax.set_xticks(list(range(0, len(baskets), basket_increment)))

    for i, p in enumerate(prods):
        ts_prod = [b["timestamp"] + 0.2 for b in baskets if p in b["products"]]
        ax.scatter(ts_prod, [1 + 0.2 * i] * len(ts_prod), s=30)

    for b in signature["blocs"]:
        ax.axvline(x=int(b[0]) - 0.2 + 1, c="xkcd:grey", linestyle="solid", linewidth=2.3)

    ax.set_ylim(0.9, 1 + 0.2 * len(prods))
    ax.set_xlim(0.8, len(baskets)+1)

    return fig,ax