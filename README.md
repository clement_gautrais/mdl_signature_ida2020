# mdl_signature_ida2020

Repository containing the code for the paper "Widening for MDL-based Retail Signature Discovery" submitted at IDA 2020


# Usage
## Setting up Python
The code is using Python 3 (any Python 3 should do, but the version I am using is 3.7).

Make sure that all required libraries, listed in requirements.txt are installed

To install the required libraries, run `pip install -r requirements.txt`

## Downloading Instacart dataset
To download the Instacart dataset, and preprocess it, run the file src/data/make_dataset.py: `python src/data/make_dataset.py`

## Launching the Instacart experiment
To launch the Instacart experiment, run the file src/experiments/run_mdl_encoding_comparison.py: `python src/experiments/run_mdl_encoding_comparison.py`

The results of the experiment should be created in the folder experiments_results.

Parameters of this experiment can be changed in the file src/experiments/expe_parameters/mdl_encoding_comparison_base.json

## Launching the generated customers experiment
To launch the generated customers experiment, run the file src/experiments/run_generated_cust_mdl_analysis.py: `python src/experiments/run_generated_cust_mdl_analysis.py`

The results of the experiment should be created in the folder experiments_results.

Parameters of this experiment can be changed in the file src/experiments/expe_parameters/generated_cust_mdl_analysis.json

## Warning on the experiments duration
To get the full experiments results, one might have to wait for several hours.
Experiments can be stopped at any time, but they will only represent partial results compared to the one in the paper.

## Plotting the results of the experiments
To plot the results of the experiments, use the provided notebooks.

You have to run all cells in the order of the notebook, and change the parameter `expe_params["experiments_dir"]` to match the name of your experiments results directory.